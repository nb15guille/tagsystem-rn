import React, { useState } from 'react';
import { StyleSheet, View, Platform, TouchableOpacity } from 'react-native';

import ParsedText from 'react-native-parsed-text';
import tagRecomendations from './data/tagRecomendations.json';
import { ArticleContainer, KeyboardAvoid, ArticleContent, ArticleLogo, ArticleInput, ArticleItemSeparator, ArticleRecomendationText, ArticleRecomendationList} from './Styles/componentStyles.js';

export default function ArticleCreator() {

  const [articleText, setArticleText] = useState('');
  const [openRecomendations, setOpenRecomendations] = useState(false);

    const RecomendationTagItem = (item: string, key: number) => {
        return (
        <View key={key}>
            <TouchableOpacity onPress={(): void => selectTag(item)}>
                <ArticleRecomendationText>
                    #{item}
                </ArticleRecomendationText>
            </TouchableOpacity>
            <ArticleItemSeparator />
        </View>
        );
    };

    const Recomendations = () => {
        if(openRecomendations) {
            var tagEntries = articleText.match(/#((\w|[\u00C0-\uFFDF])+)/g);
            if(tagEntries) {
                let lastTag = tagEntries[tagEntries.length-1].replace('#','');
                return (
                    <ArticleRecomendationList>
                        {
                            tagRecomendations.tags.filter(tag => tag.includes(lastTag)).map(RecomendationTagItem)
                        }
                    </ArticleRecomendationList>
                )
            }
        }
    }

    function lookForTags(changedText: string) {
        setArticleText(changedText);
        let lastChar = changedText.slice(-1);
        
        if(lastChar === " ") {
            setOpenRecomendations(false);

        }
        if(lastChar === "#") {
            setOpenRecomendations(true);
        }
    }

  
    function selectTag(tag: string) {
        let lastTag = articleText.lastIndexOf('#');
        setArticleText(articleText.substring(0, lastTag) + "#" + tag + articleText.substring(lastTag + tag.length));
        setOpenRecomendations(false);
    }

    return (
        <KeyboardAvoid
            behavior={Platform.OS === "ios" ? "padding" : "height"}
        >
            <ArticleContainer>
                <ArticleContent>
                    <ArticleLogo
                        resizeMode='contain'
                        source={
                            require('./assets/logo.png')
                        }
                    />
                    <ArticleInput multiline={true} blurOnSubmit placeholder="¿Qué está pasando?"
                        onChangeText={
                            (changedText: string) => lookForTags(changedText)
                        }
                    >
                        <ParsedText
                            parse={
                                [
                                    {pattern: /#((\w|[\u00C0-\uFFDF])+)/g, style: styles.article_highlightedText},
                                    {pattern: /@(\w){1,15}/, style: styles.article_highlightedText}
                                ]
                            }
                            childrenProps={{allowFontScaling: false}}
                        >
                            {articleText}
                        </ParsedText>
                    </ArticleInput>
                </ArticleContent>
                {Recomendations()}
            </ArticleContainer>
        </KeyboardAvoid>
    );
}

const styles = StyleSheet.create({
    article_highlightedText: {
        color: '#368EFF'
    }
});
