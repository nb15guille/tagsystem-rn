import React from 'react'
import styled from 'styled-components/native'

export const KeyboardAvoid = styled.KeyboardAvoidingView`
  flex: 1;
`;

export const ArticleContainer = styled.View`
  flex: 1;
`;

export const ArticleContent = styled.View`
  flex-direction: row;
  width: 100%;
  padding-horizontal: 10px;
  padding-vertical: 10px;
`;

export const ArticleLogo = styled.Image`
  height: 50px;
  width: 50px;
  flex: 1;
`;

export const ArticleInput = styled.TextInput`
  font-size: 20px;
  margin-left: 5px;
  flex: 8;
  align-self: center;
`;

export const ArticleItemSeparator = styled.View`
  height: 0.5px;
  width: 100%;
  background-color: #C8C8C8;
`;

export const ArticleRecomendationText = styled.Text`
  padding: 10px;
  font-weight: bold;
  font-size: 15px;
`;

export const ArticleRecomendationList = styled.ScrollView`
  width: 100%;
  height: 100%;
`;