import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, SafeAreaView } from 'react-native';

import ArticleCreator from './src/components/ArticleCreator/ArticleCreator';

export default function App() {

  return (
    <SafeAreaView style={styles.safeView}>
      <ArticleCreator />
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeView: {
    flex: 1
  },
});
